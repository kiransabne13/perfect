package dialenglish.in.co.dialenglish;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ResetPasswordActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText mEmailEditText;
    private Button mResetPasswordButton;
    private ProgressBar mResetPasswordProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(ResetPasswordActivity.this, NavigationActivity.class));
            finish();
        }


        mEmailEditText = findViewById(R.id.emailEditText);
        mResetPasswordButton = findViewById(R.id.resetPasswordButton);
        mResetPasswordProgressBar = findViewById(R.id.resetPasswordProgressBar);
        mResetPasswordProgressBar.setVisibility(View.INVISIBLE);
        mResetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPasswordTask();
            }
        });

    }

    private void resetPasswordTask() {
        mResetPasswordProgressBar.setVisibility(View.VISIBLE);
        dismissKeyboard(ResetPasswordActivity.this);
        final String email = ((EditText) findViewById(R.id.emailEditText)).getText().toString();

        if (email.length() == 0){
            mResetPasswordProgressBar.setVisibility(View.INVISIBLE);
            mEmailEditText.setError("Please enter your Email here");
            return;
        }

        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(ResetPasswordActivity.this, "Reset password Code has been emailed to " + email, Toast.LENGTH_LONG).show();
                    startActivity(new Intent(ResetPasswordActivity.this, LoginActivity.class));
                    finish();
                } else {
                    Log.e("Reset Password Error", task.getException().toString());
                    Toast.makeText(ResetPasswordActivity.this, "There is a Problem with Password Reset Function currently, kindly try again later", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mResetPasswordProgressBar.setVisibility(View.INVISIBLE);
    }

    private void dismissKeyboard(ResetPasswordActivity resetPasswordActivity) {
        InputMethodManager imm = (InputMethodManager) resetPasswordActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != resetPasswordActivity.getCurrentFocus())
            imm.hideSoftInputFromWindow(resetPasswordActivity.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }
}
