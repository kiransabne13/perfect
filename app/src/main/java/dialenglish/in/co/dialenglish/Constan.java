package dialenglish.in.co.dialenglish;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Constan {
    @SuppressLint("SimpleDateFormat")
    public static String getTime() {
        return new SimpleDateFormat("dd MMM yyyy , HH.mm").format(new Date());
    }
}