package dialenglish.in.co.dialenglish;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sinch.android.rtc.SinchError;

import dialenglish.in.co.dialenglish.quiz.ArrangeWords;
import dialenglish.in.co.dialenglish.quiz.QuizActivity;
import dialenglish.in.co.dialenglish.quiz.QuizHomeActivity;
import dialenglish.in.co.dialenglish.videolecture.VideoMainActivity;

public class NavigationActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener{
    FirebaseAuth mAuth;
    private TextView mUserNameNavTextView, mUserName, mUserEmail;
    private Button mFirstNavButton, mSecondNavButton, mThirdNavButton, mFouthNavButton, mFifthNavButton;
    private SharedPreferences sharedPref;
    GridView androidGridView;

    String[] gridViewString = {
            "Custom Translation", "Dictionary",
            "Lectures", "Expert Call",
            "Quizz", "Word Scramble",
    } ;
    int[] gridViewImageId = {
            R.drawable.languages_64_icon, R.drawable.dictionary_64_icon,
            R.drawable.lecture_64_icon, R.drawable.videocall_64_icon,
            R.drawable.exam_64_icon, R.drawable.test_64_icon,
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
        View headerview = navigationView.getHeaderView(0);
        final TextView username = (TextView) headerview.findViewById(R.id.userName);
        final TextView useremail = (TextView) headerview.findViewById(R.id.userEmail);

        //Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = mAuth.getCurrentUser();
     //   Log.d("User", user.toString());
        if (user == null) {
            startActivity(new Intent(NavigationActivity.this, LoginActivity.class));
            finish();
        }
        String userName = mAuth.getUid();
        String PREF_KEY = "Sinch";

        sharedPref = getSharedPreferences("Sinch",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("firebasekey", userName);
        editor.commit();

       // sharedPref = getPreferences(MODE_PRIVATE);
        sharedPref = getSharedPreferences("Sinch", MODE_PRIVATE);
        String UserId = sharedPref.getString("firebasekey", "");
        Log.d("Pref GET UserID", UserId);
        //assign ids here
        mUserNameNavTextView = findViewById(R.id.userNameNavTextView);

        //Get user Display Name from Database
        String url = "https://english-4ba26.firebaseio.com/users/"+ userName +"/Display Name";
        DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(url);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    dataSnapshot.getValue();
                    Log.d("Value", dataSnapshot.getValue().toString());
                    mUserNameNavTextView.setText("HI, Welcome " + dataSnapshot.getValue().toString());
                    username.setText(dataSnapshot.getValue().toString());
                } else {
                    mUserNameNavTextView.setText("Not Logged In, Please check Network");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mUserNameNavTextView.setText("Network Error. Try Again Later");
            }
        });

        //get user email from database

        String emailUrl = "https://english-4ba26.firebaseio.com/users/"+ userName +"/Email";
        DatabaseReference emailUrlRef = FirebaseDatabase.getInstance().getReferenceFromUrl(emailUrl);

        emailUrlRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    dataSnapshot.getValue();
                    useremail.setText(dataSnapshot.getValue().toString());
                } else {
                    useremail.setText("Network Error");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                useremail.setText("Network Error");
            }
        });

        CustomGridViewActivity adapterViewAndroid = new CustomGridViewActivity(NavigationActivity.this, gridViewString, gridViewImageId);
        androidGridView=(GridView)findViewById(R.id.gridview);
        androidGridView.setAdapter(adapterViewAndroid);
        androidGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {
             //   Toast.makeText(NavigationActivity.this, "GridView Item: " + gridViewString[+i], Toast.LENGTH_LONG).show();
             //   Log.d("i", String.valueOf(i) +" "+ String.valueOf(id));
                //start with 0 position
                if (i == 0){
                    startActivity(new Intent(NavigationActivity.this, MainActivity.class));
                }
                else if (i == 1){
                    startActivity(new Intent(NavigationActivity.this, DictionaryActivity.class));
                }
                else if (i == 2){
                    startActivity(new Intent(NavigationActivity.this, VideoMainActivity.class));
                }
                else if (i == 3){
                    startActivity(new Intent(NavigationActivity.this, CallActivity.class));
                }
                else if (i == 4){
                    startActivity(new Intent(NavigationActivity.this, QuizHomeActivity.class));
                }
                else if (i == 5){
                    startActivity(new Intent(NavigationActivity.this, ArrangeWords.class));
                }
            }
        });
        //

        //update user with latest token
        updateToken();
    }

    private void updateToken() {
        sharedPref = getSharedPreferences("Sinch", MODE_PRIVATE);
        String refreshedToken = sharedPref.getString("tokenId", "");
        Log.d("Pref Token", refreshedToken);
            String userUid = mAuth.getCurrentUser().getUid();
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            ref.child("users").child(userUid).child("tokenId").setValue(refreshedToken);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_video_call) {
            startActivity(new Intent(NavigationActivity.this, CallActivity.class));
            // Handle the camera action
        } else if (id == R.id.nav_dictionary) {
            startActivity(new Intent(NavigationActivity.this, DictionaryActivity.class));

        } else if (id == R.id.nav_videoMain) {
            startActivity(new Intent(NavigationActivity.this, VideoMainActivity.class));

        } else if (id == R.id.nav_quiz) {
            startActivity(new Intent(NavigationActivity.this, QuizHomeActivity.class));

        } else if (id == R.id.nav_share) {
            Toast.makeText(NavigationActivity.this, "Will be updated in next update", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_send) {
            Toast.makeText(NavigationActivity.this, "Will be Added in Next Update", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_to_mainpage) {
            startActivity(new Intent(NavigationActivity.this, MainActivity.class));
            finish();

        } else if (id == R.id.nav_to_logout) {
            logout();

            startActivity(new Intent(NavigationActivity.this, LoginActivity.class));
            finish();
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(NavigationActivity.this, ProfileActivity.class));

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        mAuth.signOut();
        sharedPref = getSharedPreferences("Sinch",MODE_PRIVATE);
        sharedPref.edit().clear().commit();

    }

    //add update FCM Instance Token in Users DB Firebase



}
