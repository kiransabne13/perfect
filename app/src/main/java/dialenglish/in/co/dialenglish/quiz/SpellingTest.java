package dialenglish.in.co.dialenglish.quiz;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.Collections;

import dialenglish.in.co.dialenglish.R;

public class SpellingTest extends AppCompatActivity {

    final StringBuilder spellingBuilder = new StringBuilder();
    private TextView mSpellingTextView, mAnswerTextView, mTimerTextView;
    private int mQuestionNumber = 0;
    String wordString;
    private Button mBackButton, mCloseButton;
    CountDownTimer mTimer;
    private LinearLayout root;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spelling_test);

        mAnswerTextView = findViewById(R.id.answerTextView);
        mSpellingTextView = findViewById(R.id.spellingTextView);
        mBackButton = findViewById(R.id.backButton);
        mCloseButton = findViewById(R.id.closeButton);
        mTimerTextView = findViewById(R.id.timerTextView);
        root = (LinearLayout) findViewById(R.id.letterLinearLayout);
        //final LinearLayout root = (LinearLayout) findViewById(R.id.letterLinearLayout);

        AlertDialog.Builder notifybuilder = new AlertDialog.Builder(SpellingTest.this);
        notifybuilder.setTitle("Introduction Tip");
        notifybuilder.setMessage("This is Spelling Quiz, You will have to Guess Correct English Word from the Word given in Marathi and tap on letters appearing on lower part of screen to form a Correct English Word and it will be viewed on top");
        notifybuilder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        notifybuilder.show();

       updateWords();

       mBackButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               mTimer.cancel();
               startActivity(new Intent(SpellingTest.this, QuizHomeActivity.class));
               finish();
           }
       });

       mCloseButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               mTimer.cancel();
               startActivity(new Intent(SpellingTest.this, EndQuizActivity.class));
               finish();
           }
       });

    }

    private void updateWords() {
        //Quiz Timer
        mTimerTextView = findViewById(R.id.timerTextView);

        mTimer = new CountDownTimer(20000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                System.out.println("seconds remaining: " + millisUntilFinished / 1000);
                mTimerTextView.setText(String.valueOf(millisUntilFinished / 1000));

                if ((millisUntilFinished / 1000) < 4){
                    mTimerTextView.setTextColor(getResources().getColor(R.color.colorRed));

                }
            }

            @Override
            public void onFinish() {
                System.out.println("Done");
                mTimerTextView.setText("Time Up");
                mTimerTextView.setTextColor(getResources().getColor(R.color.colorWhite));
                //Aleart Dialog
                AlertDialog.Builder timeupAlert = new AlertDialog.Builder(SpellingTest.this);
                timeupAlert.setTitle("Time Up");
                timeupAlert.setMessage("Sorry, You haven't completed Quiz in qiven time seconds." );
                timeupAlert.setPositiveButton("Next", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(SpellingTest.this, "Next Question is on the way", Toast.LENGTH_SHORT).show();
                        updateWords();
                        root.removeAllViews();
                    }
                });
                timeupAlert.show();
                //TODO when on direct shutdown or app going background stop the timer counter
            }
        }.start();



        DatabaseReference questionCount = FirebaseDatabase.getInstance().getReference();
        questionCount.child("spellingQuiz").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getChildrenCount();
            //    Log.d("Children Count", String.valueOf(dataSnapshot.getChildrenCount()));
                if (mQuestionNumber <= dataSnapshot.getChildrenCount() - 2) {
                    mQuestionNumber++;

                } else {
                    Toast.makeText(SpellingTest.this, "No more Words are added yet", Toast.LENGTH_LONG).show();
                    mTimer.cancel();
                    root.removeAllViews();
                    startActivity(new Intent(SpellingTest.this, EndQuizActivity.class));
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //get words

        String wordUrl = "https://english-4ba26.firebaseio.com/spellingQuiz/"+ mQuestionNumber +"/word";
        DatabaseReference wordRef = FirebaseDatabase.getInstance().getReferenceFromUrl(wordUrl);
        Log.d("wordUrl", wordUrl);
        wordRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String word = dataSnapshot.getValue(String.class);
                playWith(word);
//                wordString.equals(word);
            //    Log.d("word", word);
                String[] letter = word.split("");
                Collections.shuffle(Arrays.asList(letter));
             //   Log.d("Letters in Word", word);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //get marathiWords and show it in mSpellingTextView as hint for corresponding words.

        String marathiWordUrl = "https://english-4ba26.firebaseio.com/spellingQuiz/"+ mQuestionNumber +"/marathiWord";
        DatabaseReference marathiWordRef = FirebaseDatabase.getInstance().getReferenceFromUrl(marathiWordUrl);
        Log.d("marathiWordUrl", marathiWordUrl);
        marathiWordRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String marathiWord = dataSnapshot.getValue(String.class);
                    mSpellingTextView.setText(marathiWord);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void playWith(final String word) {

        final String[] alphabets = word.split("");

        for (String alphabet : alphabets){
        }
        Collections.shuffle(Arrays.asList(alphabets));

        final TextView[] t = new TextView[alphabets.length];
        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);

        for ( final String position : alphabets) {
            t[position.length()] = new TextView(this);
            t[position.length()].setLayoutParams(dim);
            t[position.length()].setText(position);
            t[position.length()].setTextColor(getResources().getColor(R.color.colorPrimary));
            t[position.length()].setTextSize(20);

            t[position.length()].setTypeface(Typeface.DEFAULT_BOLD);
            root.addView(t[position.length()]);
            t[position.length()].setPadding(20,10,10,10);
            t[position.length()].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    spellingBuilder.append(position);
                //    Log.d("Builder", spellingBuilder.toString());
                    mAnswerTextView.append(position);
                    mAnswerTextView.setText(spellingBuilder.toString());
                    String result = spellingBuilder.toString();
                //    Log.d("Speel", mAnswerTextView.getText().toString());
     //               check(result);
                    if (result.equals(word)){
                        mTimer.cancel();
                        AlertDialog.Builder correctAlertBuilder = new AlertDialog.Builder(SpellingTest.this);
                        correctAlertBuilder.setTitle("You are Correct");
                        correctAlertBuilder.setMessage("Great!! You have Spelled it correctly. " + word.toUpperCase());
                        correctAlertBuilder.setPositiveButton("Okay Next", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                updateWords();
                            }
                        });
                        correctAlertBuilder.show();
                        spellingBuilder.setLength(0);
                        spellingBuilder.trimToSize();
                       // updateWords();
                        mAnswerTextView.setText("");
                        root.removeAllViews();
                    }

                }
            });
            System.out.println(position);
        }

    }
    public void onBackPressed() {
        mTimer.cancel();
        super.onBackPressed();
    }
}
