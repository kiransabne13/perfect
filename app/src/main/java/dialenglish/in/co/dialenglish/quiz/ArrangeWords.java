package dialenglish.in.co.dialenglish.quiz;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import dialenglish.in.co.dialenglish.LoginActivity;
import dialenglish.in.co.dialenglish.NavigationActivity;
import dialenglish.in.co.dialenglish.R;

public class ArrangeWords extends AppCompatActivity {
    private ImageView mBackButton;
    private TextView mMarathiSentenceTextView, mSentenceTextView, mTimerTextView, mOption1TextView, mOption2TextView, mOption3TextView, mOption4TextView, mOption5TextView, mOption6TextView;
    private Button mResetButton, mCloseButton;
    String mcorrectSentence;
    private FirebaseAuth mAuth;
    private int mQuestionNumber = 0;
    CountDownTimer mcounter;
    private static final String WHITESPACE = " ";
    final StringBuilder builder = new StringBuilder();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arrange_words);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            mcounter.cancel();
            startActivity(new Intent(ArrangeWords.this, LoginActivity.class));
            finish();
        }

        mBackButton = findViewById(R.id.backButton);
        //backButton is a imageview
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcounter.cancel();
                startActivity(new Intent(ArrangeWords.this, QuizHomeActivity.class));
                finish();
            }
        });

        //assign values
        mMarathiSentenceTextView = findViewById(R.id.marathiSentence);
        mSentenceTextView = findViewById(R.id.sentenceTextView);
        mTimerTextView = findViewById(R.id.timerTextView);
        mOption1TextView = findViewById(R.id.option1TextView);
        mOption2TextView = findViewById(R.id.option2TextView);
        mOption3TextView = findViewById(R.id.option3TextView);
        mOption4TextView = findViewById(R.id.option4TextView);
        mOption5TextView = findViewById(R.id.option5TextView);
        mCloseButton = findViewById(R.id.closeButton);
        mTimerTextView = (TextView) findViewById(R.id.timerTextView);

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.button_click_sound);

        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.setLength(0);
                builder.trimToSize();
                mcounter.cancel();
                startActivity(new Intent(ArrangeWords.this, EndQuizActivity.class));
                finish();
            }
        });


        updateViews();

        //Create String Builder;

        //set On Click Listener on TextViews

        mOption1TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                builder.append(WHITESPACE);
                builder.append(mOption1TextView.getText());
                Log.d("builderOne", builder.toString());
                mSentenceTextView.append(mOption1TextView.getText());
                mSentenceTextView.setText(builder.toString());
                Log.d("sentence 1", mSentenceTextView.getText().toString());
                String resultString = builder.toString();
                check(resultString);
            }
        });

        mOption2TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                builder.append(WHITESPACE);
                builder.append(mOption2TextView.getText());
                Log.d("builderTwo", builder.toString());
                mSentenceTextView.append(mOption2TextView.getText());
                mSentenceTextView.setText(builder.toString());
                Log.d("Sentence 2", mSentenceTextView.getText().toString());
                String resultString = builder.toString();
                check(resultString);
            }
        });

        mOption3TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                builder.append(WHITESPACE);
                builder.append(mOption3TextView.getText());
                Log.d("builderThree", builder.toString());
                mSentenceTextView.append(mOption3TextView.getText());
                mSentenceTextView.setText(builder.toString());
                Log.d("Sentence 3", mSentenceTextView.getText().toString());
                String resultString = builder.toString();
                check(resultString);
            }
        });

        mOption4TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                builder.append(WHITESPACE);
                builder.append(mOption4TextView.getText());
                Log.d("builderFour", builder.toString());
                mSentenceTextView.append(mOption4TextView.getText());
                mSentenceTextView.setText(builder.toString());
                Log.d("Sentence 4", mSentenceTextView.getText().toString());
                String resultString = builder.toString();
                check(resultString);
            }
        });

        mOption5TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                builder.append(WHITESPACE);
                builder.append(mOption5TextView.getText());
                Log.d("builderFive", builder.toString());
                mSentenceTextView.append(mOption5TextView.getText());
                mSentenceTextView.setText(builder.toString());
                Log.d("Sentence 5", mSentenceTextView.getText().toString());
                String resultString = builder.toString();
                check(resultString);
            }
        });
    }

    private void check(String resultString) {
        if (resultString.equals(mcorrectSentence)) {
            mcounter.cancel();
            Toast.makeText(ArrangeWords.this, "Yo are Correct", Toast.LENGTH_LONG).show();

            //Aleart Dialog
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ArrangeWords.this);
            alertBuilder.setTitle("Correct");
            alertBuilder.setMessage("You are Correct. Correct English Sentence is " + resultString.toUpperCase());
            alertBuilder.setPositiveButton("Next", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(ArrangeWords.this, "Next Question is on the way", Toast.LENGTH_SHORT).show();
                    updateViews();
                }
            });
            alertBuilder.show();

            builder.setLength(0);
            builder.trimToSize();
            mSentenceTextView.setText("");


        } else {
         //   Toast.makeText(ArrangeWords.this, "Try again", Toast.LENGTH_SHORT).show();
        }
    }

    //update Questions, Sentences & options
    private void updateViews() {

        //Timer
        mTimerTextView = (TextView) findViewById(R.id.timerTextView);

        //Timer
        mcounter = new CountDownTimer(20000, 1000) {

            public void onTick(long millisUntilFinished) {
                System.out.println("seconds remaining: " + millisUntilFinished / 1000);
                mTimerTextView.setText(String.valueOf(millisUntilFinished / 1000));

                if ((millisUntilFinished / 1000) < 6){
                    mTimerTextView.setTextColor(getResources().getColor(R.color.colorRed));

                }

                //      mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                //      mTextField.setText("done!");
                System.out.println("Done");
                mTimerTextView.setText("Time Up");
                mTimerTextView.setTextColor(getResources().getColor(R.color.colorWhite));
                //Aleart Dialog
                AlertDialog.Builder timerAlertBuilder = new AlertDialog.Builder(ArrangeWords.this);
                timerAlertBuilder.setTitle("Time is Up");
                timerAlertBuilder.setMessage("You failed to complete it in given time. The Timer on above goes out. Try to be quick next time");
                timerAlertBuilder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(ArrangeWords.this, "Next Question is on the way", Toast.LENGTH_SHORT).show();
                        updateViews();
                    }
                });
                timerAlertBuilder.show();
                builder.setLength(0);
                builder.trimToSize();
                mSentenceTextView.setText("");

            }
        }.start();



        //get from Firebase Database

        //get MarathiSentence
        String marathiurl = "https://english-4ba26.firebaseio.com/arrangeWords/" + mQuestionNumber + "/marathiSentence";
        DatabaseReference marathiref = FirebaseDatabase.getInstance().getReferenceFromUrl(marathiurl);

        marathiref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String marathiSentence = dataSnapshot.getValue(String.class);
                mMarathiSentenceTextView.setText(marathiSentence);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mMarathiSentenceTextView.setText("No Network. Try again later");
            }
        });


        //get QuestionNumber
        DatabaseReference questionCount = FirebaseDatabase.getInstance().getReference();
        questionCount.child("arrangeWords").addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getChildrenCount();
                Log.d("Children Count", String.valueOf(dataSnapshot.getChildrenCount()));
                if (mQuestionNumber <= dataSnapshot.getChildrenCount() - 2) {
                    mQuestionNumber++;
                //    mEndButton.setVisibility(View.VISIBLE);
                } else {
                 //   mEndButton.setVisibility(View.INVISIBLE);
                    mcounter.cancel();
                    Toast.makeText(ArrangeWords.this, "We will update Quiz with new Questions, Try again later", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ArrangeWords.this, EndQuizActivity.class));
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //get Option1

        String option1url = "https://english-4ba26.firebaseio.com/arrangeWords/" + mQuestionNumber + "/optionWord1";
        DatabaseReference option1Ref = FirebaseDatabase.getInstance().getReferenceFromUrl(option1url);
        Log.d("Option 1 Url", option1url);
        option1Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String option1 = dataSnapshot.getValue(String.class);
                mOption1TextView.setText(option1);
                mOption1TextView.setBackground(getResources().getDrawable(R.drawable.spelling_quiz_options_border));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //get Option 2

        String option2url = "https://english-4ba26.firebaseio.com/arrangeWords/" + mQuestionNumber + "/optionWord2";
        DatabaseReference option2Ref = FirebaseDatabase.getInstance().getReferenceFromUrl(option2url);

        option2Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String option2 = dataSnapshot.getValue(String.class);
                mOption2TextView.setText(option2);
                mOption2TextView.setBackground(getResources().getDrawable(R.drawable.spelling_quiz_options_border));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //get Option 3

        String option3url = "https://english-4ba26.firebaseio.com/arrangeWords/" + mQuestionNumber + "/optionWord3";
        DatabaseReference option3Ref = FirebaseDatabase.getInstance().getReferenceFromUrl(option3url);

        option3Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String option3 = dataSnapshot.getValue(String.class);
                mOption3TextView.setText(option3);
                mOption3TextView.setBackground(getResources().getDrawable(R.drawable.spelling_quiz_options_border));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        String option4url = "https://english-4ba26.firebaseio.com/arrangeWords/" + mQuestionNumber + "/optionWord4";
        DatabaseReference option4Ref = FirebaseDatabase.getInstance().getReferenceFromUrl(option4url);

        option4Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String option4 = dataSnapshot.getValue(String.class);
                mOption4TextView.setText(option4);
                mOption4TextView.setBackground(getResources().getDrawable(R.drawable.spelling_quiz_options_border));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        String option5url = "https://english-4ba26.firebaseio.com/arrangeWords/" + mQuestionNumber + "/optionWord5";
        DatabaseReference option5Ref = FirebaseDatabase.getInstance().getReferenceFromUrl(option5url);

        option5Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String option5 = dataSnapshot.getValue(String.class);
                mOption5TextView.setText(option5);
                mOption5TextView.setBackground(getResources().getDrawable(R.drawable.spelling_quiz_options_border));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //get correctSentence

        String correctSentenceUrl = "https://english-4ba26.firebaseio.com/arrangeWords/" + mQuestionNumber + "/correctSentence";
        DatabaseReference correctSentenceRef = FirebaseDatabase.getInstance().getReferenceFromUrl(correctSentenceUrl);

        correctSentenceRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String correctSentence = dataSnapshot.getValue(String.class);
                mcorrectSentence = WHITESPACE + correctSentence;
                Log.d("mCorrectSentence", correctSentence);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }
    public void onBackPressed() {
        mcounter.cancel();
        super.onBackPressed();
    }

}
