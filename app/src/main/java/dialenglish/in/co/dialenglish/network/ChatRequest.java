package dialenglish.in.co.dialenglish.network;


import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import dialenglish.in.co.dialenglish.AppController;
import dialenglish.in.co.dialenglish.model.ModelChat;

import static android.content.Context.MODE_PRIVATE;

public class ChatRequest {
    private SharedPreferences sharedPref;

    public static void getChat(final OnChatRequest chatResult) {

        String adminPair = AppController.getInstance().getSharedPreferences().getString("AdminPair", "");
        String nodePair = AppController.getInstance().getSharedPreferences().getString("NodePair", "");
       // String adminUID = AppController.getInstance().getSharedPreferences().getString("AdminUID", "");
//
//        String adminPair = getIntent().getStringExtra("adminPair");
//        Log.d("AdminPair", adminPair);
//
//        String nodePair = getIntent().getStringExtra("nodePair");
//        Log.d("NodePair", nodePair);
//
//        String adminUID = getIntent().getStringExtra("adminUID");
//        Log.d("AdminUID", adminUID);

        String nodeChatURl = "https://english-4ba26.firebaseio.com/chatNode/"+ nodePair;

        DatabaseReference reference = FirebaseDatabase.getInstance().getReferenceFromUrl(nodeChatURl);
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ModelChat chat = dataSnapshot.getValue(ModelChat.class);
                chatResult.result(chat);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public static void postMessage(ModelChat chat){


        String adminPair = AppController.getInstance().getSharedPreferences().getString("AdminPair", "");
        String nodePair = AppController.getInstance().getSharedPreferences().getString("NodePair", "");
    //    String adminUID = AppController.getInstance().getSharedPreferences().getString("AdminUID", "");

        String adminChaturl = "https://english-4ba26.firebaseio.com/adminChatNode/"+adminPair;
        DatabaseReference reference = FirebaseDatabase.getInstance().getReferenceFromUrl(adminChaturl);


        //updating user node
        String userChaturl = "https://english-4ba26.firebaseio.com/chatNode/"+nodePair;
        DatabaseReference userNodeRef = FirebaseDatabase.getInstance().getReferenceFromUrl(userChaturl);
        String keyChat = reference.push().getKey();
        reference.child(keyChat).setValue(chat);
        userNodeRef.child(keyChat).setValue(chat);

    }

    public interface OnChatRequest {
        void result(ModelChat chat);
    }
}
