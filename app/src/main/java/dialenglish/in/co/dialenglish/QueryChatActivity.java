package dialenglish.in.co.dialenglish;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class QueryChatActivity extends BaseActivity {
    FirebaseAuth mAuth;
    private ListView mChatListView;
    private EditText mMessageEditText;
    private Button mSendButton;
    public ArrayList<String> arr;
    public ArrayAdapter adapter;
    String userNameValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query_chat);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            startActivity(new Intent(QueryChatActivity.this, MainActivity.class));
            finish();
        }

        mMessageEditText = findViewById(R.id.messageEditText);
        mSendButton = findViewById(R.id.sendButton);
        mChatListView = findViewById(R.id.chatListView);
        final ArrayList<String> chatMessages = new ArrayList<String>();


        //Show Alert that this service is in testing phase & live translation help is provided between 10AM to 7PM
        AlertDialog.Builder serviceAlert = new AlertDialog.Builder(QueryChatActivity.this);
        serviceAlert.setTitle("Translation Service Alert");
        serviceAlert.setMessage("This feature of Realtime Translation help service is in testing phase. The Service will be available from 10AM to 7PM. Shortly this will be updated with full Service");
        serviceAlert.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(QueryChatActivity.this, "Thanks", Toast.LENGTH_SHORT).show();
            }
        });
        serviceAlert.show();

        FirebaseUser user = mAuth.getCurrentUser();
        final String fromUserUid = user.getUid();

        //Get FromUser Name from database with fromUserUid
        String getUserNameUrl = "https://english-4ba26.firebaseio.com/users/"+fromUserUid;
        final DatabaseReference getUserNameRef = FirebaseDatabase.getInstance().getReferenceFromUrl(getUserNameUrl);
        Log.d("UserDetailsUrl", getUserNameUrl);

        getUserNameRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    userNameValue = dataSnapshot.child("Display Name").getValue().toString();
                    Log.d("data Value", userNameValue);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        String searchText = getIntent().getStringExtra("EditSearchText");
        String toDisplayName = getIntent().getStringExtra("To Display Name");
        final String toDisplayUID = getIntent().getStringExtra("To Expert UID");
        String toDisplayEmail = getIntent().getStringExtra("To Expert Email");

        Log.d("S", searchText);
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        final String adminChatList = "https://english-4ba26.firebaseio.com/adminChatList/";
        final DatabaseReference adminChatListRef = FirebaseDatabase.getInstance().getReferenceFromUrl(adminChatList);
        final String adminPair = toDisplayUID +"with" +fromUserUid;
        final String nodePair = fromUserUid + "with" +toDisplayUID;
        Log.d("nodePair 11", nodePair);

        Query pairQuery = adminChatListRef.orderByChild("adminPair").equalTo(adminPair);
        pairQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()){
                    getUserNameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String senderName = dataSnapshot.child("Display Name").getValue().toString();

                            final Map<String, String> adminListMap = new HashMap<String, String>();
                            adminListMap.put("adminPair", adminPair);
                            adminListMap.put("sender", senderName);
                            adminListMap.put("nodePair", nodePair);
                            //    Log.d("Sender", senderName);
                            adminChatListRef.push().setValue(adminListMap);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//add initial search text word in both chat nodes
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Message", searchText);
        ref.child("chatNode").child(fromUserUid + "with" +toDisplayUID).push().setValue(map);
        ref.child("adminChatNode").child(toDisplayUID + "with" + fromUserUid).push().setValue(map);

        // Create or Retrive Chat Nodes for both sides

        String nodeUrl = "https://english-4ba26.firebaseio.com/chatNode/"+fromUserUid +"with" + toDisplayUID;
        final DatabaseReference nodeRef = FirebaseDatabase.getInstance().getReferenceFromUrl(nodeUrl);

        //For Admin
        String adminNodeUrl = "https://english-4ba26.firebaseio.com/adminChatNode/"+toDisplayUID+"with"+fromUserUid;
        final DatabaseReference adminNodeRef = FirebaseDatabase.getInstance().getReferenceFromUrl(adminNodeUrl);

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSendButton.setEnabled(false);
                String newMessage = mMessageEditText.getText().toString().trim();
                Map<String, String> messageMap = new HashMap<String, String>();
                messageMap.put("Message", newMessage);
                nodeRef.push().setValue(messageMap);
                adminNodeRef.push().setValue(messageMap);
                //  nodeRef.push().setValue(newMessage);
                mMessageEditText.setText("");
                mSendButton.setEnabled(true);

            }
        });

        nodeRef.limitToLast(20).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot message : dataSnapshot.getChildren()) {
                        Log.d("Message", message.getValue().toString());
                        chatMessages.add(message.getValue().toString());
                    }
                    ArrayAdapter arrayAdapter = new ArrayAdapter(QueryChatActivity.this, android.R.layout.simple_list_item_1,
                            chatMessages);
                    mChatListView.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
