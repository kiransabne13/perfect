package dialenglish.in.co.dialenglish;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dialenglish.in.co.dialenglish.quiz.QuizActivity;
import dialenglish.in.co.dialenglish.videolecture.VideoMainActivity;

public class MainActivity extends BaseActivity {
    FirebaseAuth mAuth;
    TextView mUserNameTextView;
    private TextView mTextMessage;
    public ArrayList<String> arr;
    public ArrayAdapter adapter;
    private EditText mEditTextWord;
    String inputText;
    String searchWord, userName, emailid;
    private Button mSearchButton, mChatHistoryButton;
    private ProgressBar mProgressBar2;
    DatabaseReference rootRef,demoRef;
    FirebaseUser user;
    String paidUser = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mAuth = FirebaseAuth.getInstance();

         user = mAuth.getCurrentUser();

        if (user == null) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }
        userName = mAuth.getUid();

        // get userUid named as username in sharedpref and check it with firebase instance userUid  if

        mUserNameTextView = findViewById(R.id.userNameTextView);

        //get user emailid and put it as user during chat
        emailid = mAuth.getCurrentUser().getEmail();

        getUserNamefromdb(userName);    //getting name from the user db in firebase method

        mEditTextWord = (EditText) findViewById(R.id.editTextWord);
//        inputText = mEditTextWord.getText().toString();
//        searchWord = inputText.toLowerCase();
        mProgressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
        mProgressBar2.setVisibility(View.INVISIBLE);
        mSearchButton = findViewById(R.id.searchButton2);
        mChatHistoryButton = findViewById(R.id.chatHistoryButton);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        //        searchMeaning(inputText);
                dismissKeyboard(MainActivity.this);
            }
        });

        mChatHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         //      getReadyforChat();
             //   seeChatHistory();
                dismissKeyboard(MainActivity.this);
            }
        });

    }


    private void getUserNamefromdb(String userName) {

        String url = "https://english-4ba26.firebaseio.com/users/"+ userName +"/Display Name";
        DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(url);

        String userUrl = "https://english-4ba26.firebaseio.com/users/"+userName;
        DatabaseReference reference = FirebaseDatabase.getInstance().getReferenceFromUrl(userUrl);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    mUserNameTextView.setVisibility(View.VISIBLE);
                    paidUser = dataSnapshot.child("paidUser").getValue().toString();
                    String userDisplayName = dataSnapshot.child("Display Name").getValue().toString();
                    mUserNameTextView.setText("Hi. " + userDisplayName);

                } else {
                    mUserNameTextView.setText("Network Error, Try Logging in Again");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mUserNameTextView.setText("Network Error, Try Logging in Again");
            }
        });

    }

    //OnResume Code for the onBackPressed and onCreate method
    @Override
    public void onResume(){
        super.onResume();
        Log.d("On Resume Started", "On Resume Code Started");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mAuth = FirebaseAuth.getInstance();

        user = mAuth.getCurrentUser();

        if (user == null) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }
        userName = mAuth.getUid();

        mUserNameTextView = findViewById(R.id.userNameTextView);

        //get user emailid and put it as user during chat
        emailid = mAuth.getCurrentUser().getEmail();

        getUserNamefromdb(userName);    //getting name from the user db in firebase method

        mEditTextWord = (EditText) findViewById(R.id.editTextWord);
     //   final String inputText = mEditTextWord.getText().toString();

        mEditTextWord.setText("");
//        searchWord = inputText.toLowerCase();
        mProgressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
        mProgressBar2.setVisibility(View.INVISIBLE);
        mSearchButton = findViewById(R.id.searchButton2);
        mChatHistoryButton = findViewById(R.id.chatHistoryButton);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchMeaning(inputText);
                dismissKeyboard(MainActivity.this);
            }
        });

        mChatHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  getReadyforChat(inputText);
                   seeChatHistory();
                dismissKeyboard(MainActivity.this);
            }
        });

    }

    private void checkInput(String inputText) {
        this.inputText = mEditTextWord.getText().toString();
        if (this.inputText.split("").length > 7){
            mEditTextWord.setError("Upgrade to Paid Subscription for more than 7 Words");
            return;
        }
    }

    private void dismissKeyboard(MainActivity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus())
            imm.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getApplicationWindowToken(), 0);

    }

    private void searchMeaning(final String inputText) {
        System.out.println("Search Meaning Method " + paidUser);

        //database reference pointing to root of database
        rootRef = FirebaseDatabase.getInstance().getReference();
        //database reference pointing to demo node

        final String word = mEditTextWord.getText().toString();
        if (word.split(" ").length > 7){
            if (paidUser.equals("false")) {
                mEditTextWord.setError("Upgrade to Paid Subscription for more than 7 words");
                //Alert for asking to upgrade the subscription
                AlertDialog.Builder upgradeAlert = new AlertDialog.Builder(MainActivity.this);
                upgradeAlert.setTitle("Upgrade Subscription");
                upgradeAlert.setMessage("The Free-Tier limit only allows maximum 7 words, to remove this limit kindly upgrade the Subscription");
                upgradeAlert.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO start AccountInfo Activity
                        startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                        finish();
                    }
                });
                upgradeAlert.show();
                return;
            }
        }
        demoRef = rootRef.child("usersQuery");

        mProgressBar2.setVisibility(View.VISIBLE);
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        final ListView listView = (ListView) findViewById(R.id.listView);
        listView.setDivider(new ColorDrawable(Color.parseColor("#FFBF00")));
        listView.setDividerHeight(2);

        final ArrayList<String> itemsInList = new ArrayList<String>();
        Query query = reference.child("lang").orderByChild("textinEnglish").equalTo(word.toLowerCase());
        query.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    mProgressBar2.setVisibility(View.INVISIBLE);
                    // dataSnapshot is the "issue" node with all children with id 0
                    for (DataSnapshot lang : dataSnapshot.getChildren()) {
                     //   Log.d("DataSnapShot", lang.toString());
                        itemsInList.add(lang.child("textinMarathi").getValue().toString());

                    }
                    ArrayAdapter arrayAdapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1,
                            itemsInList);
                    listView.setAdapter(arrayAdapter);
                } else {
                    String value = word.toLowerCase();
                    final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("query", value);
                    ref.child("usersQuery").push().setValue(map);
                    Log.d("value", value);

                //    demoRef.push().setValue(value);

                    Toast.makeText(MainActivity.this, "Not Found. Database is being Updated. Our Expert will help you", Toast.LENGTH_SHORT).show();
                    mProgressBar2.setVisibility(View.INVISIBLE);
                    //intent to Chat Activity for custom translation
                    getReadyforChat(word);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("Database Error", databaseError.toString());
                Toast.makeText(MainActivity.this, "Network Error. Try Again Later", Toast.LENGTH_SHORT).show();
                mProgressBar2.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void getReadyforChat(final String word) {
        //query not found so will catch the query with Dial English Expert Details & intent it to chat translation activity with extra data

        String expertUrl = "https://english-4ba26.firebaseio.com/users/2GtzaEy6esfT9S06o6MEi7M2U642";
        DatabaseReference expertRef = FirebaseDatabase.getInstance().getReferenceFromUrl(expertUrl);

        expertRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Log.d("Expert Name", dataSnapshot.child("Display Name").getValue().toString());
                    Log.d("Expert UID", dataSnapshot.child("UserID").getValue().toString());
                    Log.d("Expert Email", dataSnapshot.child("Email").getValue().toString());
                //    Log.d("Word : ", word);

                    Intent intent = new Intent(MainActivity.this, NewQueryChatActivity.class);
                    intent.putExtra("To Display Name", dataSnapshot.child("Display Name").getValue().toString());
                    intent.putExtra("To Expert UID", dataSnapshot.child("UserID").getValue().toString());
                    intent.putExtra("To Expert Email", dataSnapshot.child("Email").getValue().toString());
                    intent.putExtra("EditSearchText", word);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void seeChatHistory(){
        //query not found so will catch the query with Dial English Expert Details & intent it to chat translation activity with extra data

        String expertUrl = "https://english-4ba26.firebaseio.com/users/2GtzaEy6esfT9S06o6MEi7M2U642";
        DatabaseReference expertRef = FirebaseDatabase.getInstance().getReferenceFromUrl(expertUrl);

        expertRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Log.d("Expert Name", dataSnapshot.child("Display Name").getValue().toString());
                    Log.d("Expert UID", dataSnapshot.child("UserID").getValue().toString());
                    Log.d("Expert Email", dataSnapshot.child("Email").getValue().toString());

                    Intent intent = new Intent(MainActivity.this, NewQueryChatActivity.class);
                    intent.putExtra("To Display Name", dataSnapshot.child("Display Name").getValue().toString());
                    intent.putExtra("To Expert UID", dataSnapshot.child("UserID").getValue().toString());
                    intent.putExtra("To Expert Email", dataSnapshot.child("Email").getValue().toString());
                    intent.putExtra("EditSearchText", "");
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//Dont use Finish(); as we want user to return to home page
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, CallActivity.class));
        }
        if (id == R.id.goToDictionary) {
            startActivity(new Intent(MainActivity.this, DictionaryActivity.class));
        }
        if (id == R.id.goToLectures) {
            startActivity(new Intent(MainActivity.this, VideoMainActivity.class));
        }
        if (id == R.id.goToQuiz) {
            startActivity(new Intent(MainActivity.this, QuizActivity.class));
        }
//        if (id == R.id.goToScrambler) {
//            startActivity(new Intent(MainActivity.this, ArrangeWords.class));
//        }
        if (id == R.id.logout) {
            logout();
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }
        if (id == R.id.goToNavigation) {
            startActivity(new Intent(MainActivity.this, NavigationActivity.class));
        }


        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        mAuth.signOut();
    }
}
