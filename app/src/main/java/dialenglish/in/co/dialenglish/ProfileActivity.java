package dialenglish.in.co.dialenglish;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import dialenglish.in.co.dialenglish.payment.ProductActivity;

public class ProfileActivity extends AppCompatActivity {
    private TextView mUserNameTextView, mUserEmailTextView, mAccountSubscriptionText;
    private ImageView mBackImageView, mSubscriptionEmoji;
    FirebaseAuth mAuth;
    private Button mGoToUpgradeButton;

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("Resumeed Activity");

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null){
            startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
            finish();
        }
        mUserNameTextView = findViewById(R.id.userNameTextView);
        mUserEmailTextView = findViewById(R.id.userEmailTextView);
        mAccountSubscriptionText = findViewById(R.id.accountSubscriptionText);
        mBackImageView = findViewById(R.id.backImageView);
        mSubscriptionEmoji = findViewById(R.id.subscriptionEmoji);
        mGoToUpgradeButton = findViewById(R.id.buttonGoToUpgrade);
        mGoToUpgradeButton.setVisibility(View.INVISIBLE);

        doStuff();

        mBackImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this, NavigationActivity.class));
                finish();
            }
        });
    }

    private void doStuff() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Getting Info");
        progressDialog.show();

        final String userUid = mAuth.getCurrentUser().getUid();
        String userUrl = "https://english-4ba26.firebaseio.com/users/"+userUid;
        System.out.println(userUrl);
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(userUrl);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    final String userName = dataSnapshot.child("Display Name").getValue().toString();
                    final String userEmail = dataSnapshot.child("Email").getValue().toString();

                        //TODO add or update firebase database users lists with other attributes with blank value, like paidUser, tokenId etc. which was not in earlier versions
                        String paidUser = dataSnapshot.child("paidUser").getValue().toString();
                        System.out.println(paidUser);
                        if (paidUser.equals("true")) {

                            String premiumUserUrl = "https://english-4ba26.firebaseio.com/premiumUsers/" + userUid;
                            DatabaseReference premiumRef = FirebaseDatabase.getInstance().getReferenceFromUrl(premiumUserUrl);
                            System.out.println(premiumUserUrl);
                            premiumRef.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        String renewalDate = dataSnapshot.child("lastDate").getValue().toString();
                                        progressDialog.dismiss();
                                        mGoToUpgradeButton.setVisibility(View.INVISIBLE);
                                        Log.d("If", "Paid User");
                                        mUserNameTextView.setText(userName);
                                        mUserEmailTextView.setText(userEmail);
                                        mSubscriptionEmoji.setImageDrawable(getResources().getDrawable(R.drawable.happy_emoji));
                                        mAccountSubscriptionText.setText("Hey! " + userName + ", Thanks for purchasing the paid subscription. Your Subscription will Expire on " + renewalDate + ".");
                                    } else {
                                        //     progressDialog.dismiss();
                                        Toast.makeText(ProfileActivity.this, "Error. Kindly Contact us for help.", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Toast.makeText(ProfileActivity.this, "Network Error. Kindly Try Again Later", Toast.LENGTH_SHORT).show();

                                }
                            });


                        } else {
                            //paid user is false for that user
                            progressDialog.dismiss();
                            mGoToUpgradeButton.setVisibility(View.VISIBLE);
                            mUserNameTextView.setText(userName);
                            mUserEmailTextView.setText(userEmail);
                            mSubscriptionEmoji.setImageDrawable(getResources().getDrawable(R.drawable.confused_welcome_emoji));
                            mAccountSubscriptionText.setText("Hi! " + userName + ", How was your experience with the App? Kindly upgrade your Subscription and Learn English Speaking with us, with our Premium course. ");

                            mGoToUpgradeButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(ProfileActivity.this, ProductActivity.class));
                                }
                            });
                        }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(ProfileActivity.this, "Network Error. Kindly Try Again Later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = mAuth.getCurrentUser();
        //   Log.d("User", user.toString());
        if (user == null) {
            startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
            finish();
        }

        mUserNameTextView = findViewById(R.id.userNameTextView);
        mUserEmailTextView = findViewById(R.id.userEmailTextView);
        mAccountSubscriptionText = findViewById(R.id.accountSubscriptionText);
        mBackImageView = findViewById(R.id.backImageView);
        mSubscriptionEmoji = findViewById(R.id.subscriptionEmoji);
        mGoToUpgradeButton = findViewById(R.id.buttonGoToUpgrade);

    }
}
