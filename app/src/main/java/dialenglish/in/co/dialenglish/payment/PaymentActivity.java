package dialenglish.in.co.dialenglish.payment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.easebuzz.payment.kit.PWECouponsActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import datamodels.StaticDataModel;
import dialenglish.in.co.dialenglish.LoginActivity;
import dialenglish.in.co.dialenglish.ProfileActivity;
import dialenglish.in.co.dialenglish.R;

public class PaymentActivity extends AppCompatActivity {
    private TextView mSelectedPackageText;
    private Button mMakePaymentButton;
    FirebaseAuth mAuth;
    String monthlyPackage = "Monthly Package";
    String quarterlyPackage = "Quarterly Package";
    String halfYearly = "Half Yearly Package";
    String yearlyPackage = "Yearly Package";
    String startDate, lastDate;

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("Resumed Activity");
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(PaymentActivity.this, LoginActivity.class));
            finish();
        }

        mSelectedPackageText = findViewById(R.id.selectedPackageTextView);
        mMakePaymentButton = findViewById(R.id.makePaymentButton);
        mMakePaymentButton.setVisibility(View.INVISIBLE);
        String userUid = mAuth.getCurrentUser().getUid();

        getUserDetails();
    }

    private void getUserDetails() {
        final String userUid = String.valueOf(mAuth.getCurrentUser().getUid());
        String url = "https://english-4ba26.firebaseio.com/users/"+ userUid;
        DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(url);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    final String displayName = dataSnapshot.child("Display Name").getValue().toString();
                    final String userEmail = dataSnapshot.child("Email").getValue().toString();
                    final String isSubscribed = dataSnapshot.child("paidUser").getValue().toString();
                    String packageSelected = getIntent().getStringExtra("Package Selected");

                    if (isSubscribed.equals("false") && packageSelected.equals(monthlyPackage)){
                        mMakePaymentButton.setVisibility(View.VISIBLE);
                        mSelectedPackageText.setText("You have Selected Monthly Subscription Package");
                        mMakePaymentButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //get details for making payments
                                Date currentTime = Calendar.getInstance().getTime();
                                String expiryDate = "";
                                //get expiry date in return statement

                                expiryDate = generateExpiryDate(expiryDate);
                                startDate = currentTime.toString();
                                lastDate = expiryDate;
                                Log.d("Expiary Date:  ", expiryDate);

                                Log.d("currentTime", currentTime.toString());
                                // Thu Jul 19 12:51:01 GMT+05:30 2018
                                Random rand = new Random();
                                int num = rand.nextInt(9000000) + 1000000;
                                String merchant_trxnId = String.valueOf(num);

                                Float merchant_payment_amount = Float.valueOf("300");
                                String merchant_productInfo = "Monthly Dial English Subscription";
                                String customer_firstName = displayName;
                                String customer_email_id = userEmail;
                                int merchant_is_coupon_enabled = 0;
                                String customers_unique_id = "";
                                String payment_mode = "production";
                                String merchant_key = "0UKIQJNRZU";
                                String merchant_salt = "3BAMQSEJCH";
                                String customer_phone = "9876543210";

                                makePayment(startDate, lastDate, merchant_trxnId, merchant_payment_amount, merchant_productInfo, customer_firstName, customer_email_id, merchant_is_coupon_enabled, customers_unique_id, payment_mode, merchant_key, merchant_salt, customer_phone);


                                Toast.makeText(PaymentActivity.this, "User Subscription is " + isSubscribed+ "Package selected is " + monthlyPackage+" .", Toast.LENGTH_SHORT).show();
                            }
                        });


                    }else if (isSubscribed.equals("false") && packageSelected.equals(quarterlyPackage)){
                        mMakePaymentButton.setVisibility(View.VISIBLE);
                        mSelectedPackageText.setText("You have Selected Quarterly Package");
                        mMakePaymentButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //get details for making payments
                                Date currentTime = Calendar.getInstance().getTime();
                                String expiryDate = "";
                                //get expiry date in return statement

                                expiryDate = generateQuarterlyExpiryDate(expiryDate);
                                startDate = currentTime.toString();
                                lastDate = expiryDate;
                                Log.d("Expiary Date:  ", expiryDate);

                                Log.d("currentTime", currentTime.toString());
                                // Thu Jul 19 12:51:01 GMT+05:30 2018
                                Random rand = new Random();
                                int num = rand.nextInt(9000000) + 1000000;
                                String merchant_trxnId = String.valueOf(num);

                                Float merchant_payment_amount = Float.valueOf("700");
                                String merchant_productInfo = "Quarterly Dial English Subscription";
                                String customer_firstName = displayName;
                                String customer_email_id = userEmail;
                                int merchant_is_coupon_enabled = 0;
                                String customers_unique_id = "";
                                String payment_mode = "production";
                                String merchant_key = "0UKIQJNRZU";
                                String merchant_salt = "3BAMQSEJCH";
                                String customer_phone = "9876543210";

                                makePayment(startDate, lastDate, merchant_trxnId, merchant_payment_amount, merchant_productInfo, customer_firstName, customer_email_id, merchant_is_coupon_enabled, customers_unique_id, payment_mode, merchant_key, merchant_salt, customer_phone);
                                Toast.makeText(PaymentActivity.this, "Quarterly Package Selected", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else if (isSubscribed.equals("false") && packageSelected.equals(halfYearly)) {
                        mMakePaymentButton.setVisibility(View.VISIBLE);
                        mSelectedPackageText.setText("You have Selected Half Yearly Package");
                        mMakePaymentButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //get details for making payments
                                Date currentTime = Calendar.getInstance().getTime();
                                String expiryDate = "";
                                //get expiry date in return statement

                                expiryDate = generate6MonthsExpiryDate(expiryDate);
                                startDate = currentTime.toString();
                                lastDate = expiryDate;
                                Log.d("Expiary Date:  ", expiryDate);

                                Log.d("currentTime", currentTime.toString());
                                // Thu Jul 19 12:51:01 GMT+05:30 2018
                                Random rand = new Random();
                                int num = rand.nextInt(9000000) + 1000000;
                                String merchant_trxnId = String.valueOf(num);

                                Float merchant_payment_amount = Float.valueOf("1200");
                                String merchant_productInfo = "Half-Yearly Dial English Subscription";
                                String customer_firstName = displayName;
                                String customer_email_id = userEmail;
                                int merchant_is_coupon_enabled = 0;
                                String customers_unique_id = "";
                                String payment_mode = "production";
                                String merchant_key = "0UKIQJNRZU";
                                String merchant_salt = "3BAMQSEJCH";
                                String customer_phone = "9876543210";

                                makePayment(startDate, lastDate, merchant_trxnId, merchant_payment_amount, merchant_productInfo, customer_firstName, customer_email_id, merchant_is_coupon_enabled, customers_unique_id, payment_mode, merchant_key, merchant_salt, customer_phone);
                                Toast.makeText(PaymentActivity.this, "Half-Yearly Package Selected", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else if (isSubscribed.equals("false") && packageSelected.equals(yearlyPackage)){

                        mMakePaymentButton.setVisibility(View.VISIBLE);
                        mSelectedPackageText.setText("You have Selected Yearly Package");
                        mMakePaymentButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //get details for making payments
                                Date currentTime = Calendar.getInstance().getTime();
                                String expiryDate = "";
                                //get expiry date in return statement

                                expiryDate = generateYearlyExpiryDate(expiryDate);
                                startDate = currentTime.toString();
                                lastDate = expiryDate;
                                Log.d("Expiary Date:  ", expiryDate);
                                Log.d("currentTime", currentTime.toString());
                                // Thu Jul 19 12:51:01 GMT+05:30 2018
                                Random rand = new Random();
                                int num = rand.nextInt(9000000) + 1000000;
                                String merchant_trxnId = String.valueOf(num);

                                Float merchant_payment_amount = Float.valueOf("2000");
                                String merchant_productInfo = "Yearly Dial English Subscription";
                                String customer_firstName = displayName;
                                String customer_email_id = userEmail;
                                int merchant_is_coupon_enabled = 0;
                                String customers_unique_id = "";
                                String payment_mode = "production";
                                String merchant_key = "0UKIQJNRZU";
                                String merchant_salt = "3BAMQSEJCH";
                                String customer_phone = "9876543210";

                                makePayment(startDate, lastDate, merchant_trxnId, merchant_payment_amount, merchant_productInfo, customer_firstName, customer_email_id, merchant_is_coupon_enabled, customers_unique_id, payment_mode, merchant_key, merchant_salt, customer_phone);
                                Toast.makeText(PaymentActivity.this, "Yearly Package Selected", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private String generateYearlyExpiryDate(String expiryDate) {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        //adding six months to current date
        cal.add(Calendar.MONTH, 12);
        java.util.Date expirationDate = cal.getTime();

        String expirationdate = expirationDate.toString();
        return expirationdate;
    }

    private String generate6MonthsExpiryDate(String expiryDate) {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        //adding six months to current date
        cal.add(Calendar.MONTH, 6);
        java.util.Date expirationDate = cal.getTime();

        String expirationdate = expirationDate.toString();
        return expirationdate;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(PaymentActivity.this, LoginActivity.class));
            finish();
        }

        mSelectedPackageText = findViewById(R.id.selectedPackageTextView);
        mMakePaymentButton = findViewById(R.id.makePaymentButton);
        mMakePaymentButton.setVisibility(View.INVISIBLE);
        String userUid = mAuth.getCurrentUser().getUid();

        ////////////////////////////////////////////


        // get users details and check if he is a paid user, if not then show pay button and then initaite payment



 //           String expiryDate = "";
    //        expiryDate = generateExpiryDate(expiryDate);
        }

    private String generateQuarterlyExpiryDate(String expiryDate) {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        //adding six months to current date
        cal.add(Calendar.MONTH, 3);
        java.util.Date expirationDate = cal.getTime();

        String expirationdate = expirationDate.toString();
        return expirationdate;
    }

    private void makePayment(String startDate, String lastDate, String merchant_trxnId, Float merchant_payment_amount, String merchant_productInfo, String customer_firstName, String customer_email_id, int merchant_is_coupon_enabled, String customers_unique_id, String payment_mode, String merchant_key, String merchant_salt, String customer_phone) {
        Log.d("Details for Payment", startDate + lastDate + merchant_trxnId + merchant_payment_amount + merchant_productInfo + customer_firstName + customer_email_id + merchant_is_coupon_enabled + customers_unique_id);
        //intent to easebuzz payment gateway

        Intent intentProceed = new Intent(getBaseContext(), PWECouponsActivity.class);
        intentProceed.putExtra("trxn_id",merchant_trxnId);
        intentProceed.putExtra("trxn_amount",merchant_payment_amount);
        intentProceed.putExtra("trxn_prod_info",merchant_productInfo);
        intentProceed.putExtra("trxn_firstname",customer_firstName);
        intentProceed.putExtra("trxn_email_id",customer_email_id);
        intentProceed.putExtra("trxn_phone",customer_phone);
        intentProceed.putExtra("trxn_key",merchant_key);
        intentProceed.putExtra("trxn_is_coupon_enabled",merchant_is_coupon_enabled);
        intentProceed.putExtra("trxn_salt",merchant_salt);
        intentProceed.putExtra("unique_id",customers_unique_id);
        intentProceed.putExtra("pay_mode",payment_mode);
        startActivityForResult(intentProceed, StaticDataModel.PWE_REQUEST_CODE);

    }

    private String generateExpiryDate(String expiryDate) {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        //adding six months to current date
        cal.add(Calendar.MONTH, 1);
        java.util.Date expirationDate = cal.getTime();

        String expirationdate = expirationDate.toString();
        return expirationdate;
    }


    private void checkIfPaidUser(String userUid) {

    }

    //on result code for payment gateway

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            String result = data.getStringExtra("result");
            String response = data.getStringExtra("payment_response");
            Log.d("Result", result);
            Log.d("Response", response);
            String payment_response = data.getStringExtra("result");
            String easepayid = data.getStringExtra("easepayid");

            try {
                if (result.equals("payment_successfull")) {
                    System.out.println(startDate);
                    System.out.println(lastDate);

                    //Generate Toast with next renewal date
                    //add response details to premiumUsers DB list
                    saveResponseDetails(response, startDate, lastDate);

                    //Show Alert that payment is successful and the next renewal date with user name. get user name from sharedPref
                    AlertDialog.Builder serviceAlert = new AlertDialog.Builder(PaymentActivity.this);
                    serviceAlert.setTitle("Payment Successful");
                    serviceAlert.setMessage("Thanks. The Payment is Successful. Your Account next renewal date is " + lastDate);
                    serviceAlert.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(PaymentActivity.this, "Thanks", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(PaymentActivity.this, ProfileActivity.class));
                            finish();
                        }
                    });
                    serviceAlert.show();

                    //set paidUser attribute to true in Users db for that uid

                    //Log.d("Name in Alert", mAuth.getCurrentUser().getDisplayName());
                }
            } catch (Exception e) {
                e.getStackTrace();

            }

            try {
                if (result.contains(StaticDataModel.TXN_SUCCESS_CODE)) {
                    //   showSuccessActivity(payment_response);

                } else if (result.contains(StaticDataModel.TXN_ERROR_RETRY_FAILED_CODE)) {
                    showFailActivity(payment_response);

                } else if (result.contains(StaticDataModel.TXN_ERROR_NO_RETRY_CODE)) {
                    //showFailActivity(payment_response);
                } else if (result.contains(StaticDataModel.TXN_TIMEOUT_CODE)) {
                    //    showFailActivity(payment_response);
                } else if (result.contains(StaticDataModel.TXN_BACKPRESSED_CODE)) {
                    //   showFailActivity(payment_response);
                } else if (result.contains(StaticDataModel.TXN_USERCANCELLED_CODE)) {
                    //   showFailActivity(payment_response);
                } else if (result.contains(StaticDataModel.TXN_ERROR_SERVER_ERROR_CODE)) {
                    //    showFailActivity(payment_response);
                } else if (result.contains(StaticDataModel.TXN_USER_FAILED_CODE)) {
                    //   showFailActivity(payment_response);
                } else {
                    //  showFailActivity(payment_response);
                }
            } catch (Exception e) {

            }
        } else {
            Toast.makeText(this, "Payment transaction has been canceled.", Toast.LENGTH_LONG).show();//Could not receive data
        }
    }

    private void showFailActivity(String payment_response) {
        Toast.makeText(PaymentActivity.this, "Failed to Process Payment. "+payment_response, Toast.LENGTH_SHORT).show();
    }

    private void saveResponseDetails(String response, String startDate, String lastDate) {
        String premiumUserUrl = "https://english-4ba26.firebaseio.com/premiumUsers/";
        DatabaseReference premiumUsersRef = FirebaseDatabase.getInstance().getReferenceFromUrl(premiumUserUrl);

        FirebaseUser user = mAuth.getCurrentUser();
        Map<String, String> map = new HashMap<String, String>();
        map.put("UserID", mAuth.getCurrentUser().getUid());
        map.put("pResponse", response);
        map.put("startDate", startDate);
        map.put("lastDate", lastDate);
        premiumUsersRef.child(user.getUid()).setValue(map);

        String userUid = mAuth.getCurrentUser().getUid();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.child("users").child(userUid).child("paidUser").setValue("true");
    }

}
