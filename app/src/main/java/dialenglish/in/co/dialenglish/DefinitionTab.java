package dialenglish.in.co.dialenglish;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DefinitionTab extends Fragment {
    private EditText medittext;
    private Button mbutton;
    public ArrayList<String> arr;
    public ArrayAdapter adapter;
    private ListView mDefinitionListView;
    private ProgressBar mProgressBar2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.definition_tab,container,false);

        medittext = view.findViewById(R.id.editText);
        mbutton = view.findViewById(R.id.btnSendData);
        mDefinitionListView = view.findViewById(R.id.definitionListView);
        mProgressBar2 = (ProgressBar) view.findViewById(R.id.progressBar2);
        mProgressBar2.setVisibility(View.INVISIBLE);

        mbutton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View v) {

                String word = medittext.getText().toString();
                if(word.length() == 0){
                    //   mEditUserName.setError("please input user name");
                    Toast.makeText(getActivity().getApplicationContext(), "Please Enter Input for Search", Toast.LENGTH_SHORT).show();
                    return;
                }
                //passing word to synonyms fragment
                SynonymsTab fragment = new SynonymsTab();
                Bundle bundle = new Bundle();
                bundle.putString("word", medittext.getText().toString());
                fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.second_frag,fragment).commit();
                //Fire Volley request and Get Data from Wordapi
                getDetails(word);
                dismissKeyboard(DefinitionTab.this);

            }
        });

        return view;
    }

    private void dismissKeyboard(DefinitionTab definitionTab) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != getActivity().getCurrentFocus())
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }

    private void getDetails(final String word) {
        mProgressBar2.setVisibility(View.VISIBLE);
        final ArrayList<String> itemsInList = new ArrayList<String>();
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String uri = Uri.parse("https://wordsapiv1.p.mashape.com/words/"+word)
                .buildUpon()
                .build().toString();

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("MainActivity", "response: " + response);
                Log.d("Word", word);

                //Extracting JSON objects from  Response
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("results");
                    for(int i=0; i<jsonArray.length(); i++){
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//                        JSONObject jsonword = jsonObject1.getJSONObject("definition");
                        String definition = jsonObject1.getString("definition");
                        String partOfSpeech = jsonObject1.getString("partOfSpeech");

                        //    String synonyms = jsonObject1.getString("synonyms");
                        if (jsonObject1.has("synonyms")) {
                            String synonyms = jsonObject1.getString("synonyms");
                            Log.d("String with Synonyms", definition + synonyms);
                        }
                        Log.d("Result", definition + partOfSpeech);
                        //   Log.d("Definition", String.valueOf(jsonword));
                        //   Log.d("String Definition", definition);
                        //   wordDetailsArrayList.add(wordDetails);
                        mProgressBar2.setVisibility(View.INVISIBLE);

                        itemsInList.add(definition+" ("+partOfSpeech+")");

                        // itemsInList.add(synonyms);

                    }
                    mProgressBar2.setVisibility(View.INVISIBLE);

                    ArrayAdapter arrayAdapter = new ArrayAdapter(getContext().getApplicationContext(), android.R.layout.simple_list_item_1, itemsInList){
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View view =super.getView(position, convertView, parent);

                            TextView textView=(TextView) view.findViewById(android.R.id.text1);
                            textView.setTextColor(getResources().getColor(R.color.colorBlack));

                            return view;
                        }
                    };
                    mDefinitionListView.setAdapter(arrayAdapter);


                } catch (JSONException e) {
                    mProgressBar2.setVisibility(View.INVISIBLE);

                    Toast.makeText(getContext().getApplicationContext(), "Network Error, Try Again Later", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("MainActivityVolleyError", error.toString());
                mProgressBar2.setVisibility(View.INVISIBLE);
                Toast.makeText(getContext().getApplicationContext(), "Error Could Not be Found, Try Again Later", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("X-Mashape-Key", "HLYn1iRKyJmshb6jqUejMvh9wbnep1LvPwPjsn8kAMbpFzcozb");
                params.put("Accept", "text/plain");
                return params;
            }
        };
        queue.add(stringRequest);

    }
}
