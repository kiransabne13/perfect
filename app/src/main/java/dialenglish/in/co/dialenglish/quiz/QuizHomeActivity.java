package dialenglish.in.co.dialenglish.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

import dialenglish.in.co.dialenglish.LoginActivity;
import dialenglish.in.co.dialenglish.NavigationActivity;
import dialenglish.in.co.dialenglish.R;

public class QuizHomeActivity extends AppCompatActivity {
    private Button mArrangeWordButton, mQuizActivityButton, mMainButton, mSpellingQuizButton;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            startActivity(new Intent(QuizHomeActivity.this, LoginActivity.class));
            finish();
        }

        Log.d("MAuth", mAuth.toString());

        mArrangeWordButton = findViewById(R.id.arrangeWordsQuizButton);
        mQuizActivityButton = findViewById(R.id.quizActivityButton);
        mMainButton = findViewById(R.id.mainButton);
        mSpellingQuizButton = findViewById(R.id.spellingQuizButton);

        mArrangeWordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(QuizHomeActivity.this, ArrangeWords.class));
                finish();
            }
        });

        mQuizActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(QuizHomeActivity.this, QuizActivity.class));
                finish();
            }
        });

        mMainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(QuizHomeActivity.this, NavigationActivity.class));
                finish();
            }
        });

        mSpellingQuizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(QuizHomeActivity.this, SpellingTest.class));
                finish();
            }
        });


    }

}
