package dialenglish.in.co.dialenglish.quiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import dialenglish.in.co.dialenglish.LoginActivity;
import dialenglish.in.co.dialenglish.MainActivity;
import dialenglish.in.co.dialenglish.NavigationActivity;
import dialenglish.in.co.dialenglish.R;

public class EndQuizActivity extends AppCompatActivity {
    private TextView mTextMessage;
    FirebaseAuth mAuth;
    private Button mGoToMainPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_quiz);

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() == null) {
            startActivity(new Intent(EndQuizActivity.this, LoginActivity.class));
        }

        mGoToMainPage = findViewById(R.id.goToMainPage);

        mGoToMainPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EndQuizActivity.this, QuizHomeActivity.class));
                finish();
            }
        });
    }
}
