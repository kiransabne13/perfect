package dialenglish.in.co.dialenglish.adapter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;
import dialenglish.in.co.dialenglish.AppController;
import dialenglish.in.co.dialenglish.R;
import dialenglish.in.co.dialenglish.model.ModelChat;

public class HolderChat extends RecyclerView.ViewHolder {
    private SharedPreferences sharedPref;
FirebaseAuth mAuth;

    @BindViews({R.id.cardFrom, R.id.cardTo})
    List<CardView> cardChat;

    @BindViews({R.id.fromUsername, R.id.toUsername})
    List<TextView> username;

    @BindViews({R.id.fromMessage, R.id.toMessage})
    List<TextView> message;

    @BindViews({R.id.fromTime, R.id.toTime})
    List<TextView> time;

    public HolderChat(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setContent(ModelChat chat) {

        String User = chat.getUser();


        mAuth = FirebaseAuth.getInstance();


        String user = mAuth.getCurrentUser().getEmail(); //AppController.getInstance().getSharedPreferences().getString("user", "");

        int POS_FROM = 0;
        int POS_TO = 1;

        if (User.equals(user)) {

            cardChat.get(POS_TO).setVisibility(View.VISIBLE);
            cardChat.get(POS_FROM).setVisibility(View.GONE);

            username.get(POS_TO).setText(chat.getUser());
            message.get(POS_TO).setText(chat.getMessage());
            time.get(POS_TO).setText(chat.getTime());


        } else {

            cardChat.get(POS_TO).setVisibility(View.GONE);
            cardChat.get(POS_FROM).setVisibility(View.VISIBLE);

            username.get(POS_FROM).setText(chat.getUser());
            message.get(POS_FROM).setText(chat.getMessage());
            time.get(POS_FROM).setText(chat.getTime());
        }
    }

}

