package dialenglish.in.co.dialenglish;


import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SynonymsTab extends Fragment {
    private TextView txtWord;
    public ArrayList<String> arr;
    public ArrayAdapter adapter;
    private ListView mSynonymsListView;
    private ListView mOppositesListView;
    private TextView mOppositeWordTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.synonyms_tab, container, false);

        TextView txtWord = (TextView) view.findViewById(R.id.textView);
        mSynonymsListView = (ListView) view.findViewById(R.id.synonymsListView);
        mOppositesListView = (ListView) view.findViewById(R.id.oppositesListView);
        mOppositeWordTextView = (TextView) view.findViewById(R.id.oppositesTextView);

        Bundle bundle = getArguments();
        if(bundle!= null)
        {
            String value = getArguments().getString("word");
            txtWord.setText("Synonyms of " + value);
            getSynonyms(value);
            mOppositeWordTextView.setText("Opposite of "+value);
            getOpposite(value);
        }

        return view;
    }

    private void getOpposite(String value) {
        final ArrayList<String> itemsInList = new ArrayList<String>();
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String uri = Uri.parse("https://wordsapiv1.p.mashape.com/words/"+value+"/antonyms")
                .buildUpon()
                .build().toString();
        Log.d("uri", uri);

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("MainActivity", "response: " + response);

                //Extracting JSON objects from  Response
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("antonyms");
                    for(int i=0; i<jsonArray.length(); i++){
                        String jsonObject1 = jsonArray.getString(i);
//                        JSONObject jsonword = jsonObject1.getJSONObject("definition");
                        //   JSONObject synonyms = jsonObject1.getJSONArray("synonyms");
                        Log.d("Antonyms", jsonObject1.toString());
                        //    String synonyms = jsonObject1.getString("synonyms");

//                        WordDetails wordDetails = new WordDetails();
//                        wordDetails.setDefinition(jsonObject1.getString("definition"));
//                        wordDetails.setPartOfSpeech(jsonObject1.getString("partOfSpeech"));
                        //  Log.d("Result", synonyms);
                        //   Log.d("Definition", String.valueOf(jsonword));
                        //   Log.d("String Definition", definition);
                        //   wordDetailsArrayList.add(wordDetails);
                        itemsInList.add(jsonObject1.toString());

                        // itemsInList.add(synonyms);

                    }
                    ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, itemsInList);
                    // mWordListView.setAdapter(customListAdapter);
//                    customListAdapter.notifyDataSetChanged();
                    mOppositesListView.setAdapter(arrayAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("MainActivityVolleyError", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("X-Mashape-Key", "HLYn1iRKyJmshb6jqUejMvh9wbnep1LvPwPjsn8kAMbpFzcozb");
                params.put("Accept", "text/plain");
                return params;
            }
        };
        queue.add(stringRequest);

    }

    private void getSynonyms(String value) {
        final ArrayList<String> itemsInList = new ArrayList<String>();
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String uri = Uri.parse("https://wordsapiv1.p.mashape.com/words/"+value+"/synonyms")
                .buildUpon()
                .build().toString();
        Log.d("uri", uri);

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("MainActivity", "response: " + response);

                //Extracting JSON objects from  Response
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("synonyms");
                    for(int i=0; i<jsonArray.length(); i++){
                        String jsonObject1 = jsonArray.getString(i);
//                        JSONObject jsonword = jsonObject1.getJSONObject("definition");
                        //   JSONObject synonyms = jsonObject1.getJSONArray("synonyms");
                        Log.d("Synonyms", jsonObject1.toString());
                        //    String synonyms = jsonObject1.getString("synonyms");

//                        WordDetails wordDetails = new WordDetails();
//                        wordDetails.setDefinition(jsonObject1.getString("definition"));
//                        wordDetails.setPartOfSpeech(jsonObject1.getString("partOfSpeech"));
                        //  Log.d("Result", synonyms);
                        //   Log.d("Definition", String.valueOf(jsonword));
                        //   Log.d("String Definition", definition);
                        //   wordDetailsArrayList.add(wordDetails);
                        itemsInList.add(jsonObject1.toString());

                        // itemsInList.add(synonyms);

                    }
                    ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, itemsInList){
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);

                            TextView textView=(TextView) view.findViewById(android.R.id.text1);
                        //    textView.setTextColor(Color.BLACK);
                            textView.setTextColor(getResources().getColor(R.color.colorBlack));

                            return view;
                        }
                    };
                    // mWordListView.setAdapter(customListAdapter);
//                    customListAdapter.notifyDataSetChanged();
                    mSynonymsListView.setAdapter(arrayAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("MainActivityVolleyError", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("X-Mashape-Key", "HLYn1iRKyJmshb6jqUejMvh9wbnep1LvPwPjsn8kAMbpFzcozb");
                params.put("Accept", "text/plain");
                return params;
            }
        };
        queue.add(stringRequest);

    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("onResume gets called");
    }
}
