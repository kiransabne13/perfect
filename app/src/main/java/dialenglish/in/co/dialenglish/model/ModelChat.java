package dialenglish.in.co.dialenglish.model;

public class ModelChat {

    private String user;
    private String message;
    private String time;
    private String senderUid;
    private String receiverUid;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public String getSenderUid() {
        return senderUid;
    }

    public void setSenderUid(String senderUid) {
        this.senderUid = senderUid;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public String getReceiverUid() {
        return receiverUid;
    }

    public void setReceiverUid(String receiverUid) {
        this.receiverUid = receiverUid;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
