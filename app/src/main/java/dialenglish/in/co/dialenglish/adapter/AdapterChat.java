package dialenglish.in.co.dialenglish.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import dialenglish.in.co.dialenglish.R;
import dialenglish.in.co.dialenglish.model.ModelChat;

public class AdapterChat extends RecyclerView.Adapter<HolderChat> {

    private List<ModelChat> dataChat;

    public AdapterChat(List<ModelChat> dataChat) {
        this.dataChat = dataChat;
    }

    @Override
    public HolderChat onCreateViewHolder(ViewGroup parent, int viewType) {
        View v  = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat , parent , false);
        return new HolderChat(v);
    }

    @Override
    public void onBindViewHolder(final HolderChat holder, int position) {
        holder.setContent(dataChat.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(holder);

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataChat.size();
    }
}
