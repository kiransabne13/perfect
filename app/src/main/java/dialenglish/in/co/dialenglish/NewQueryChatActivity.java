package dialenglish.in.co.dialenglish;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import dialenglish.in.co.dialenglish.adapter.AdapterChat;
import dialenglish.in.co.dialenglish.model.ModelChat;
import dialenglish.in.co.dialenglish.network.ChatRequest;
import dialenglish.in.co.dialenglish.utils.EditTextListener;

public class NewQueryChatActivity extends AppCompatActivity {
    private SharedPreferences sharedPref;
    String userNameValue;
    DatabaseReference mDatabase;
    private LinearLayoutManager mLinearLayoutManager;
    int mCounter;
    int queryCounter = 0;

    FirebaseAuth mAuth;
    @BindView(R.id.mainToolbar)
    Toolbar toolbar;

    @BindView(R.id.mainDrawer)
    DrawerLayout drawerLayout;

    @BindView(R.id.btnSend)
    Button btnSend;

    @BindView(R.id.etMessage)
    EditText etMessage;

    @BindView(R.id.chatItem)
    RecyclerView chatItem;

    AdapterChat adapterChat;
    List<ModelChat> listChat = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_query_chat);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            startActivity(new Intent(NewQueryChatActivity.this, MainActivity.class));
            finish();
        }

        //Show Alert that this service is in testing phase & live translation help is provided between 10AM to 7PM
        AlertDialog.Builder serviceAlert = new AlertDialog.Builder(NewQueryChatActivity.this);
        serviceAlert.setTitle("Translation Service Alert");
        serviceAlert.setMessage("This feature of Realtime Translation help service is in testing phase. The Service will be available from 10AM to 7PM. Shortly this will be updated with full Service");
        serviceAlert.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(NewQueryChatActivity.this, "Thanks", Toast.LENGTH_SHORT).show();
            }
        });
        serviceAlert.show();

        FirebaseUser muser = mAuth.getCurrentUser();
        final String fromUserUid = muser.getUid();
        final String user = muser.getEmail();

        // if querycounter >= 50 and user is not paiduser then show error alert and return


        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("users").child(fromUserUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String paidUser = dataSnapshot.child("paidUser").getValue().toString();
                String queryCounter = dataSnapshot.child("queryCounter").getValue().toString();
                int mcounter = Integer.parseInt(queryCounter);
                if (paidUser.equals("false")) {
                    if(mcounter >= 50) {

                        System.out.println(mcounter);
                        //show alert free limit crossed
                        AlertDialog.Builder serviceAlert = new AlertDialog.Builder(NewQueryChatActivity.this);
                        serviceAlert.setTitle("Service Usage Limit Alert");
                        serviceAlert.setMessage("This feature of Realtime Translation help service is in testing phase. You have crossed the Free Usage limit of 50 translations. Kindly upgrade as a paid member, to further use this service.");
                        serviceAlert.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(NewQueryChatActivity.this, ProfileActivity.class));
                                finish();
                            }
                        });
                        serviceAlert.show();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ////////////////////////////////////////////////

        //Get FromUser Name from database with fromUserUid
        String getUserNameUrl = "https://english-4ba26.firebaseio.com/users/"+fromUserUid;
        final DatabaseReference getUserNameRef = FirebaseDatabase.getInstance().getReferenceFromUrl(getUserNameUrl);
        Log.d("UserDetailsUrl", getUserNameUrl);

        getUserNameRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    userNameValue = dataSnapshot.child("Display Name").getValue().toString();
                    Log.d("data Value", userNameValue);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        String searchText = getIntent().getStringExtra("EditSearchText");


        String toDisplayName = getIntent().getStringExtra("To Display Name");
        final String toDisplayUID = getIntent().getStringExtra("To Expert UID");
        String toDisplayEmail = getIntent().getStringExtra("To Expert Email");

        Log.d("S", searchText);
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        final String adminChatList = "https://english-4ba26.firebaseio.com/adminChatList/";
        final DatabaseReference adminChatListRef = FirebaseDatabase.getInstance().getReferenceFromUrl(adminChatList);
        final String adminPair = toDisplayUID +"with" +fromUserUid;
        final String nodePair = fromUserUid + "with" +toDisplayUID;
        Log.d("nodePair 11", nodePair);

        sharedPref = getSharedPreferences("Sinch",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("AdminPair", adminPair);
        editor.putString("NodePair", nodePair);
        editor.putString("user", user);
        editor.commit();

        Query pairQuery = adminChatListRef.orderByChild("adminPair").equalTo(adminPair);
        pairQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()){
                    getUserNameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String senderName = dataSnapshot.child("Display Name").getValue().toString();

                            final Map<String, String> adminListMap = new HashMap<String, String>();
                            adminListMap.put("adminPair", adminPair);
                            adminListMap.put("sender", senderName);
                            adminListMap.put("nodePair", nodePair);
                            adminListMap.put("mCounter", String.valueOf(1));
                            //    Log.d("Sender", senderName);
                            adminChatListRef.child(adminPair).setValue(adminListMap);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(NewQueryChatActivity.this, "Database Error", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        ////////////////////////////////////////////////////////////////////////////////////////////////

        //adding messages in chat node

                String username = user;
                String Message = searchText;
                String Time = Constan.getTime();
                String senderUid = mAuth.getCurrentUser().getUid();
                String receiverUid = "2GtzaEy6esfT9S06o6MEi7M2U642";

                ModelChat chat = new ModelChat();
                chat.setUser(username);
                chat.setMessage(Message);
                chat.setTime(Time);
                chat.setSenderUid(senderUid);
                chat.setReceiverUid(receiverUid);

                ChatRequest.postMessage(chat);
                updateCounter();
                updateQueryCounter();
             //   etMessage.setText("");
                //        MyApplication.hideSoftInput(NewChatActivity.this , etMessage);

        ////////////////////////////////////////////////////////////////////////////


        ButterKnife.bind(this);

        bindUI();

        ChatRequest.getChat(new ChatRequest.OnChatRequest() {
            @Override
            public void result(ModelChat chat) {
                listChat.add(chat);

                if (listChat.size() > 100) {
                    listChat.remove(0);
                }

                adapterChat.notifyDataSetChanged();
            }
        });
    }

    private void updateQueryCounter() {
        String userUid = mAuth.getCurrentUser().getUid();
        String userUrl = "https://english-4ba26.firebaseio.com/users/"+userUid;

        final DatabaseReference userRef = FirebaseDatabase.getInstance().getReferenceFromUrl(userUrl);
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Log.d("Query Counter", dataSnapshot.child("queryCounter").getValue().toString());
                    String qCounter = dataSnapshot.child("queryCounter").getValue().toString();

                    int mcounter = Integer.parseInt(qCounter);
                    String val = String.valueOf(mcounter + 1);
                    userRef.child("queryCounter").setValue(val);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private void bindUI() {

            sharedPref = getSharedPreferences("Sinch", MODE_PRIVATE);
            final String user = mAuth.getCurrentUser().getEmail();

            adapterChat = new AdapterChat(listChat);
            mLinearLayoutManager = new LinearLayoutManager(this);

            mLinearLayoutManager.setStackFromEnd(true);

            chatItem.setHasFixedSize(true);
            chatItem.setLayoutManager(mLinearLayoutManager);
            chatItem.setAdapter(adapterChat);
     //       mLinearLayoutManager.getPosition(chatItem);


            // ModelUser user = PreferencesManager.initPreferences().getUserInfo();


            toolbar.setTitle("Translation Chat");

//            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
//            drawerLayout.addDrawerListener(toggle);
//            toggle.syncState();

            etMessage.addTextChangedListener(new EditTextListener(btnSend));
            etMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {

                }
            });

            //adding messages in chat node
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String username = user;
                    String Message = etMessage.getText().toString();
                    String Time = Constan.getTime();
                    String senderUid = mAuth.getCurrentUser().getUid();
                    String receiverUid = "2GtzaEy6esfT9S06o6MEi7M2U642";

                    ModelChat chat = new ModelChat();
                    chat.setUser(username);
                    chat.setMessage(Message);
                    chat.setTime(Time);
                    chat.setSenderUid(senderUid);
                    chat.setReceiverUid(receiverUid);

                    ChatRequest.postMessage(chat);

                    etMessage.setText("");
                            AppController.hideSoftInput(NewQueryChatActivity.this , etMessage);
                    updateCounter();
                    updateQueryCounter();

                }
            });

            //adding message counter


        }

    private void getQuery(String message) {
        String queryUrl = "https://english-4ba26.firebaseio.com/query/";
        DatabaseReference queryRef = FirebaseDatabase.getInstance().getReferenceFromUrl(queryUrl);

        Map<String, String> map = new HashMap<String, String>();
        map.put("query", message);
        map.put("reply", "reply");
        queryRef.push().setValue(map);

    }

    private void updateCounter() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        sharedPref = getSharedPreferences("Sinch", MODE_PRIVATE);
        final String adminPair = sharedPref.getString("AdminPair","");
            //get recent counter values from adminchatnode
        DatabaseReference counterRef = mDatabase.child("adminChatList").child(adminPair);
        counterRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                 //   Log.d("NewChatQuery", dataSnapshot.getValue().toString());
                    Log.d("NewChatQueryCounter", dataSnapshot.child("mCounter").getValue().toString());
                    String counter = dataSnapshot.child("mCounter").getValue().toString();

                    int mcounter = Integer.parseInt(counter);
                    Log.d("MCOUNTER", String.valueOf(mcounter));
                    String val = String.valueOf(mcounter + 1);
                    Log.d("VALUE", val);
                    mDatabase.child("adminChatList").child(adminPair).child("mCounter").setValue(val);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //TODO add onBackpressed code and delete sharedPref values of chat nodes
        @Override
        public void onBackPressed(){
            super.onBackPressed();
            sharedPref = getSharedPreferences("adminapp", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.remove("AdminPair");
            editor.remove("NodePair");
            editor.remove("AdminUID");
            editor.commit();
        }
    }

