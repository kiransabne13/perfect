package dialenglish.in.co.dialenglish;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sinch.android.rtc.SinchError;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends BaseActivity implements SinchService.StartFailedListener {
    private EditText mEmailEditText, mPasswordEditText, mNameEditText;
    private FirebaseAuth mAuth;
    private Button mRegisterButton;
    private ProgressBar mSignupProgressBar;
    private TextView mGoToLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //asking for permissions here
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.READ_PHONE_STATE},100);
            }
        }

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(SignupActivity.this, NavigationActivity.class));
            finish();
        }

        mGoToLogin = findViewById(R.id.goToLogin);

        mSignupProgressBar = findViewById(R.id.signupProgressBar);
        mSignupProgressBar.setVisibility(View.INVISIBLE);
        mNameEditText = findViewById(R.id.nameEditText);
        mEmailEditText = findViewById(R.id.emailEditText);
        mPasswordEditText = findViewById(R.id.passwordEditText);
        mRegisterButton = findViewById(R.id.registerButton);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRegisterButton.setEnabled(false);
                registerUser();
            }
        });

        mGoToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLogin();
            }
        });
    }

    private void goToLogin() {
        startActivity(new Intent(SignupActivity.this, LoginActivity.class));
        finish();
    }

    //this method is invoked when the connection is established with the SinchService
    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
    }

    private void registerUser() {
        mSignupProgressBar.setVisibility(View.VISIBLE);
        final String email = mEmailEditText.getText().toString().trim();
        final String password = mPasswordEditText.getText().toString().trim();
        final String name = mNameEditText.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            mSignupProgressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            mSignupProgressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.length() < 4) {
            mSignupProgressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(name)) {
            mSignupProgressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(), "Enter Your Name", Toast.LENGTH_SHORT).show();
        }

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Toast.makeText(SignupActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();

                if (task.isSuccessful()) {
                    saveUserDetails(mAuth.getCurrentUser().getUid(), name, email);
                    toNextPage();
                } else {
                    mSignupProgressBar.setVisibility(View.INVISIBLE);
                    mRegisterButton.setEnabled(true);
                    Toast.makeText(SignupActivity.this, "Registration failed." + task.getException(),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveUserDetails(String uid, String name, String email) {
        FirebaseUser user = mAuth.getCurrentUser();
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        Map<String, String> map = new HashMap<String, String>();
        map.put("Display Name", name);
        map.put("UserID", uid);
        map.put("Email", email);
        map.put("tokenId", "");
        map.put("paidUser", "false");
        map.put("queryCounter","0");
        ref.child("users").child(user.getUid()).setValue(map);
    }

    private void toNextPage() {
        // username is email id
        mSignupProgressBar.setVisibility(View.VISIBLE);
        String userName = mAuth.getUid();

        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(userName);
        } else {
            mSignupProgressBar.setVisibility(View.INVISIBLE);
            startActivity(new Intent(SignupActivity.this, NavigationActivity.class));

            finish();
        }
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();

    }

    @Override
    public void onStarted() {
toNextPage();
    }

}
