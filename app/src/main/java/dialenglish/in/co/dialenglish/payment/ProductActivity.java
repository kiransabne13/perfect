package dialenglish.in.co.dialenglish.payment;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import dialenglish.in.co.dialenglish.LoginActivity;
import dialenglish.in.co.dialenglish.R;

public class ProductActivity extends AppCompatActivity {
    private List<Product> productList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ProductsAdapter mAdapter;
    FirebaseAuth mAuth;
    RecyclerTouchListener mRecyclerTouchListener;

    private void createLists() {
        mAdapter = new ProductsAdapter(productList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        preparePackageData();
        recyclerView.addOnItemTouchListener (new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Product product = productList.get(position);
                Toast.makeText(getApplicationContext(), product.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
                String packageSelected = product.getTitle();
                if(packageSelected.equals("Monthly Package")){
                    Intent intent = new Intent(ProductActivity.this, PaymentActivity.class);
                    intent.putExtra("Package Selected", "Monthly Package");
                    intent.putExtra("userUid", mAuth.getCurrentUser().getUid());

                    startActivity(intent);
                }
                if (packageSelected.equals("Quarterly Package")){
                    Intent intent = new Intent(ProductActivity.this, PaymentActivity.class);
                    intent.putExtra("Package Selected", "Quarterly Package");
                    intent.putExtra("userUid", mAuth.getCurrentUser().getUid());
                    startActivity(intent);
                }
                if (packageSelected.equals("Half Yearly Package")){
                    Intent intent = new Intent(ProductActivity.this, PaymentActivity.class);
                    intent.putExtra("Package Selected", "Half Yearly Package");
                    intent.putExtra("userUid", mAuth.getCurrentUser().getUid());
                    startActivity(intent);
                }
                if (packageSelected.equals("Yearly Package")){
                    Intent intent = new Intent(ProductActivity.this, PaymentActivity.class);
                    intent.putExtra("Package Selected", "Yearly Package");
                    intent.putExtra("userUid", mAuth.getCurrentUser().getUid());
                    startActivity(intent);
                }
                // TODO : check the packageSelected and do the intent properly for paymentGateway
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null){
            startActivity(new Intent(ProductActivity.this, LoginActivity.class));
            finish();
        }
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        AlertDialog.Builder subscriptionAlert = new AlertDialog.Builder(ProductActivity.this);
        subscriptionAlert.setTitle("Subscription Info");
        subscriptionAlert.setMessage("For Premium Access to Contents & Services, Kindly upgrade to Paid Subscription. Kindly Select on of the Package best suited for you. We donot collect any card or bank information & it is completely handled securely by payment gateway. On Expiry of the Package, you will need to manually renew it here again.");
        subscriptionAlert.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        subscriptionAlert.show();

        createLists();
    }

    private void preparePackageData() {
        Product product = new Product("Monthly Package", "To be Renewed Monthly. With all Premium Services, its nice to get to started with and experience the app.", "INR 300/Month");
        productList.add(product);

        product = new Product("Quarterly Package", "To be renewed every quarter. Comes with all Premium Services", "INR 700 per Quarter");
        productList.add(product);

        product = new Product("Half Yearly Package", "To be renewed on Half-yearly basis.", "INR 1200 for Half-Yearly");
        productList.add(product);

        product = new Product("Yearly Package", "Yearly Package", "INR 2000 for Yearly Package");
        productList.add(product);

        mAdapter.notifyDataSetChanged();
    }

}
